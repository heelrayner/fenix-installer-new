# IMPORTS
import datetime # for time stamps
import importlib # for dynamic imports                                    
import os # for deleting files
import threading # for multithreading
import subprocess # for running commands and scripts
import gi # Python GObject introspection module which contains Python bindings and support for Gtk
gi.require_version('Gtk', '3.0') # make sure that the Gtk version is at the required level
from gi.repository import Gtk # Gtk related modules for the graphical interface
from gi.repository import GdkPixbuf # Gtk module that describes images
from gi.repository import GLib # for using a function that will allow updating the on-screen console text
from gi.repository import Pango # module for rendering formatted text
from util.configuration import JSONOptions

class InstallerPage(Gtk.Box):

    """Create a box to host the GUI elements"""

    # CONSTRUCTOR
    def __init__(self, currentPageName, mainBuilder):
        """Constructor to run the tasks that have to be accomplished at the beginning of every installer page.
        
        Parameters
        ----------
        currentPageName : string
            Name of the current installer page
        mainBuilder : Gtk.Builder()
            The Builder object associated with the main window. This could be used to access UI objects within the main installer window.
        """
        Gtk.Box.__init__(self)                                                         # call the super-class constructor        
        self.mainBuilder                  = mainBuilder                                # get access to the UI of the main window
        self.consoleBuffer                = mainBuilder.get_object("consoleTextView").get_buffer() # get access to the provided console buffer
        self.__formFilePath, self.__GtkID = self.__getPageInfo(currentPageName)        # read configuration files to find out information about the page
        self.builder                      = self.__getFormObjects(self.__formFilePath) # get form objects (in the form of a Gtk builder) from the form file
        self.__addFormObjects(self.builder, self.__GtkID)                              # add the form objects to the current page's container
        self.__trapString = "trap \'printf \"[%s %s %s] %s\\n\" $(date \'\"\'\"\'+%F, %T %Z\'\"\'\"\') \"$BASH_COMMAND\"\' DEBUG" # bash trap command to display inputs and outputs with time stamps. Multiple nested quotes have been escaped.
        self.threads = []                                                              # starting with an empty pool of threads
        self.logFile = []                                                              # no log file created initially

    @staticmethod
    def __getPageInfo(currentPageName):   
        """Lookup and return details of the current page after using the current page name to search.
        
        Parameters
        ----------
        currentPageName : string
            Name of the current page
        
        Returns
        -------
        formFilePath : string
            The path to the glade file that describes how the current page is supposed to look.
        GtkID : string
            The ID of the Gtk.Box container that describes this page. This would have already been assigned in the glade file.
        """

        pageData = JSONOptions("config/pages.json").getData()                                    #                                        
        formFilePath = os.path.join("ui", "gtk", "forms", pageData["pages"][currentPageName]["file"] + ".glade") # get the UI file path for the current page
        GtkID = pageData["pages"][currentPageName]["Gtk_ID"] # get the Gtk ID for the UI file for the top-level container in this page
        return formFilePath, GtkID

    @staticmethod
    def __getFormObjects(formFilePath):
        """Returns access to the UI objects of the current page given the path to the page's glade file that describes how the current page is supposed to look.
        
        Parameters
        ----------
        formFilePath : string
            The path to the glade file that describes how the current page is supposed to look.
        
        Returns
        -------
        builder : Gtk.Builder()
            The Builder object associated with the current page. This could be used to access UI objects within the current page.
        """
        builder = Gtk.Builder()             # 
        builder.add_from_file(formFilePath) # create Gtk objects from the UI file
        return builder

    def __addFormObjects(self, builder, containerID):
        """Looks up the specified container represented by containerID within the builder and adds it to the super class (which is a Gtk.Box)

        Parameters
        ----------
        builder : Gtk.Builder
            The given Gtk.Builder in which the container represented by containerID has to be looked up.
        containerID : string
            The container ID which has to be looked up in the specified Gtk.Builder.
        """
        page = builder.get_object(containerID) # get the Gtk container for this page
        self.pack_start(page, True, True, 0) # add the extracted GUI elements to the current box  

    def appendToConsoleText(self, text):
        """Adds text to be displayed at the end of the GUI console
        
        Parameters
        ----------
        text : string
            The text to be displayed
        """
        self.consoleBuffer.insert(self.consoleBuffer.get_end_iter(), text) # update the GUI console with additional text

    @staticmethod
    def getTimeStamp():
        """Returns the current time stamp in a specific format
        
        Returns
        -------
        string
            The current time-stamp in a specific format.
        """
        return "[" + datetime.datetime.now().strftime("%Y-%m-%d, %H:%M:%S ") + datetime.datetime.now(datetime.timezone.utc).astimezone().tzname() + "]"

    def waitForCommandsToFinish(self):
        """Suspends the current thread until all threads finish running
        """
        for thread in self.threads:           #
            thread.join()                     # wait for all running threads to finish

    def openLogFile(self):
        """Creates a log file with the current time stamp suffix and opens it for appending
        """
        fileName = "log" + "/" + datetime.datetime.now().strftime("%Y-%m-%d_%H%M%S") + "_" + datetime.datetime.now(datetime.timezone.utc).astimezone().tzname() + ".log"
        self.logFile = open(fileName, mode='a', buffering=1)

    def closeLogFile(self):
        """Closes the current log file.
        """
        self.logFile.close()

    def runShellScript(self, scriptPath):
        """Runs a shell script (that should exist on scriptPath) on a new thread and adds the new thread to the pool of threads
        
        Parameters
        ----------
        scriptPath : string
            Path to the script that has to be run.
        """
        thread = threading.Thread(target=self.__shellScriptWorker, args = (scriptPath,)) # create a new thread and pass the script path to it
        self.threads.append(thread)                                                     # add the thread to the pool of threads
        thread.start()                                                                  # start the thread

    def runShellCommand(self, commandString):
        """Runs a shell command on a new thread and adds the new thread to the pool of threads
        
        Parameters
        ----------
        commandString : string
            The command that has to be run
        """
        thread = threading.Thread(target=self.__shellCommandWorker, args = (commandString,)) # create a new thread and pass the command string to it
        self.threads.append(thread)                                                         # add the thread to the pool of threads
        thread.start()                                                                      # start the thread

    def __shellScriptWorker(self, scriptPath):
        """Runs a shell script (that should exist on scriptPath)
        
        Parameters
        ----------
        scriptPath : string
            Path to the script that has to be run.
        """
        with open(scriptPath + "_temp", "w") as script:
            with open(scriptPath, "r") as script_old:
                script.write(self.__trapString + "\n\n") # create a temporary script with a trap command to display both inputs and outputs, with time stamps              
                script.write(script_old.read())          # copy all lines from the script to a temporary script
        
        process = subprocess.Popen("sh " + scriptPath + "_temp", stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) # run the temporary script (with a trap command) such that both the output and errors go to stdout
        while True:                        
            output = str(process.stdout.readline().strip().decode()) + ""
            if output == '' and process.poll() is not None:
                break                                                     # command completed
            if output:                                                  
                self.logFile.write(output + "\n")                         # the output is written to the log file
                GLib.idle_add(self.appendToConsoleText, output + "\n")    # the output as it comes is added to the GUI console  
        
        os.remove(scriptPath + "_temp") # remove the temporary script

    def __shellCommandWorker(self, commandString):    
        """Runs a shell command
        
        Parameters
        ----------
        commandString : string
            The command that has to be run
        """
        input = self.getTimeStamp() + " " +  commandString
        self.logFile.write(input + "\n")                    # the input is written to the log file
        GLib.idle_add(self.appendToConsoleText, input + "\n") # the input is written to the console
        process = subprocess.Popen(commandString, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True) # run the command such that both the output and errors go to stdout
        while True:                        
            output = str(process.stdout.readline().strip().decode()) + ""
            if output == '' and process.poll() is not None:
                break                                                     # command completed
            if output:                   
                self.logFile.write(output + "\n")                         # the output is written to the log file      
                GLib.idle_add(self.appendToConsoleText, output + "\n")    # the output as it comes is added to the GUI console       

class PageTools():
    @staticmethod
    def addPages(listOfPages, pageStack, mainBuilder, installer_data):
        """Adds pages specified by listOfPages to the given page stack and updates and returns the installer page data (that was originally extracted from the installer configuration file)
        
        Parameters
        ----------
        listOfPages : list(string)
            List of page names
        pageStack : Gtk.Stack
            Widget that stores all the UI for the pages
        mainBuilder : Gtk.Builder
            The Builder object associated with the main window. This could be used to access UI objects within the main installer window.
        installer_data : dict
            The installer page data (that was originally extracted from the installer configuration file)
        
        Returns
        -------
        installer_data : dict
            The installer page data updated with the newly added pages
        """
        for pageName in listOfPages:
            mod = importlib.import_module("ui.gtk.code" + "." + installer_data["pages"][pageName]["file"]) # dynamically import the necessary python file for the page
            page = mod.Page(mainBuilder) # use the corresponding python script of the page to initialize it
            page.set_name(pageName) # name the page for later access
            pageStack.add_titled(page, pageName, installer_data["pages"][pageName]["title"]) # add the imported page to the Gtk stack
        installer_data["added_pages"] = listOfPages
        return installer_data

    @staticmethod
    def removePages(listOfPages, pageStack, installer_data):
        """Removes pages specified by listOfPages from the given page stack and updates and returns the installer page data (that was originally extracted from the installer configuration file)
        
        Parameters
        ----------
        listOfPages : list(string)
            List of page names
        pageStack : Gtk.Stack
            Widget that stores all the UI for the pages
        installer_data : dict
            The installer page data (that was originally extracted from the installer configuration file)
        
        Returns
        -------
        installer_data : dict
            The installer page data updated with the removed pages reflected
        """
        for page in pageStack.get_children():
            if page.get_name() in listOfPages:
                pageStack.remove(page)
        installer_data["added_pages"] = [pageName for pageName in installer_data["added_pages"] if pageName not in listOfPages]
        return installer_data

    @staticmethod
    def refreshPages(pageStack):
        """Forces the given page stack to update the pages shown, based on newly added or removed pages.
        
        Parameters
        ----------
        pageStack : Gtk.Stack
            Widget that stores all the UI for the pages
        """
        pageStack.show_all()


    
