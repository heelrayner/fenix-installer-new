import gi # Python GObject introspection module which contains Python bindings and support for Gtk
gi.require_version('Gtk', '3.0') # make sure that the Gtk version is at the required level
from gi.repository import Pango # module for rendering formatted text

class TextTools():
    @staticmethod
    def addBasicTextTags(textBuffer):
        """Create and adds basic text tags (or formatting templates) to a given TextBuffer

        Parameters
        ----------
        textBuffer : Gtk.TextBuffer
            The text buffer of a text widget to which formatting templates are to be added
        """
        textBuffer.create_tag("bold"     , weight=Pango.Weight.BOLD)         # create a text tag for bold text
        textBuffer.create_tag("italic"   , style=Pango.Style.ITALIC)         # create a text tag for italic text
        textBuffer.create_tag("underline", underline=Pango.Underline.SINGLE) # create a text tag for underlined text