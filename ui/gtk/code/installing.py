# FENIX INSTALLER
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/reborn-os-team/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# ARTWORK
# 1. Trivoxel (https://gitlab.com/TriVoxel)

import logging
from pathlib import Path
from util.installing import InstallerJob
logger = logging.getLogger('fenix_installer'+'.'+Path(__file__).stem)

# IMPORTS
from util.configuration import JSONOptions
from ui.gtk.util.page import InstallerPage, PageTools
from ui.gtk.util.image import ImageTools
from ui.gtk.util.text import TextTools

# CUSTOM IMPORTS
import threading                                       # for multithreading
import gi                                              # Gtk related modules
gi.require_version('Gtk', '3.0')                       #       
from gi.repository import Gtk                          #

# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "installing"
# ----------- Modify this ---------- #

class Page(InstallerPage):
# create a page class derived from "InstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self, mainBuilder):
        InstallerPage.__init__(self, CURRENT_PAGE_NAME, mainBuilder) # call the super-class constructor
        self.builder.connect_signals(self)                             # connect the signals from the Gtk form to our event handlers

        # ---------- Custom code ----------- #
        stack = self.builder.get_object("installing_slideStack")
        resources = JSONOptions("config/resources.json").getData()
        self.slidePaths   = resources["slides"]
        self.slideImages  = []
        self.slidePixbufs = []
        self.aspectRatios = []
        for slidePath in self.slidePaths:            
            scrolledWindow = Gtk.ScrolledWindow.new(None, None)
            viewport = Gtk.Viewport.new(None, None)
            image = Gtk.Image.new()
            stack.add_named(scrolledWindow, slidePath)
            scrolledWindow.add(viewport)
            viewport.add(image)
            # InstallerPage.addImage(slidePath, 200, image) 
            pixbuf = ImageTools.getPixbufFromFile(slidePath)
            aspectRatio = float(pixbuf.get_width()) / pixbuf.get_height()
            self.slideImages.append(image)
            self.slidePixbufs.append(pixbuf)
            self.aspectRatios.append(aspectRatio)
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place
    def installing_onSlideResized(self, stack, rectangle):
        width  = stack.get_allocated_width()
        height = stack.get_allocated_height()
        for i in range(len(self.slidePaths)):
            if float(width)/height > self.aspectRatios[i]:
                pixbuf = ImageTools.scalePixbufByHeight(self.slidePixbufs[i], height-75, self.aspectRatios[i])
            else:
                pixbuf = ImageTools.scalePixbufByWidth(self.slidePixbufs[i], width-75, self.aspectRatios[i])
            ImageTools.addImageFromPixbuf(pixbuf, self.slideImages[i])

    def installing_onLeftClicked(self, stack):
        currentSlideIndex = self.slidePaths.index(stack.get_visible_child_name())   # Determine the index of the current visible slide
        if currentSlideIndex > 0:                                                   # If the currently visible slide is not the first one
            stack.set_visible_child_name(self.slidePaths[currentSlideIndex - 1])    # Select the previous slide name from the list and make it visible
        else:                                                                       # otherwise
            stack.set_visible_child_name(self.slidePaths[len(self.slidePaths) - 1]) # select the last slide name from the list and make it visible

    def installing_onRightClicked(self, stack):
        currentSlideIndex = self.slidePaths.index(stack.get_visible_child_name()) # Determine the index of the current visible slide
        if currentSlideIndex < (len(self.slidePaths) - 1):                        # If the currently visible slide is not the last one
            stack.set_visible_child_name(self.slidePaths[currentSlideIndex + 1])  # select the next slide name from the list and make it visible
        else:                                                                     # otherwise
            stack.set_visible_child_name(self.slidePaths[0])                      # select the first slide name from the list and make it visible

    def installing_onInstallClicked(self, button):
        dialog = self.builder.get_object("installConfirmationDialog") # 
        dialog.show_all()                                             # show the installation confirmation dialog

    def installing_onNoClicked(self, dialog):
        dialog.hide()                         # when 'no' is clicked after the user is prompted to confirm installation

    def installing_onInstallConfirmed(self, dialog):
        dialog.hide()                                                                  # hide the installation confirmation dialog
        self.builder.get_object("installing_stack").set_visible_child_name("progress") # switch to installation mode where the page shows progress bar and slides        
        self.runBeforeInstallation()
        threading.Thread(target=self.installationWorker).start()                       # start the installation on a thread separate from the GUI          

    # CUSTOM METHODS
    def runBeforeInstallation(self):
        installerSettings = JSONOptions("config/installer.json")
        installerSettings.setEntry(True, "installing") # set the configuration variable to indicate that installation is in progress
        installerSettings.dump()

        self.mainBuilder.get_object("main.box.paned.stacksidebar").set_sensitive(False)
        self.mainBuilder.get_object("main.box.bottomBar.nextButton").set_sensitive(False)
        self.mainBuilder.get_object("main.box.bottomBar.backButton").set_sensitive(False)
        self.mainBuilder.get_object("main_topBackButton").set_sensitive(False)
        self.mainBuilder.get_object("main_topNextButton").set_sensitive(False)

        self.openLogFile() # create and open a new log file

    def runAfterInstallation(self):
        self.closeLogFile()

        self.mainBuilder.get_object("main.box.paned.stacksidebar").set_sensitive(True)
        self.mainBuilder.get_object("main.box.bottomBar.nextButton").set_sensitive(True)
        self.mainBuilder.get_object("main.box.bottomBar.backButton").set_sensitive(False)
        self.mainBuilder.get_object("main_topBackButton").set_sensitive(False)
        self.mainBuilder.get_object("main_topNextButton").set_sensitive(True)

        installerSettings = JSONOptions("config/installer.json")
        installerSettings.setEntry(False, "installing") # set the configuration variable to indicate that installation is not in progress
        installerSettings.dump()       

    def installationWorker(self):
        # print("Will run a test script now...")
        # self.runShellScript("scripts_old/shell/test.sh") # run a test script
        # self.waitForCommandsToFinish()
        # print("Done running the test script!!!")
        # print("Will run a test command now...")
        # self.runShellCommand("ping -c 5 www.google.com")
        # self.waitForCommandsToFinish()
        # print("Done running the test command!!!") 

        installer_job = InstallerJob()
        installer_job.runShellCommand("ping -c 5 www.google.com")
        print("Will run a test script now...")
        installer_job.runShellScript("scripts_old/shell/test.sh") # run a test script
        print("Done running the test script!!!")
        print("Will run a test command now...")
        installer_job.runShellCommand("ping -c 5 www.google.com")
        print("Done running the test command!!!")             

        self.runAfterInstallation()