# FENIX INSTALLER
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/reborn-os-team/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Trivoxel (https://gitlab.com/TriVoxel)

# IMPORTS
from util.configuration import JSONOptions
from ui.gtk.util.page import InstallerPage, PageTools
from ui.gtk.util.image import ImageTools
from ui.gtk.util.text import TextTools

# CUSTOM IMPORTS
import gi # Python GObject introspection module which contains Python bindings and support for Gtk
gi.require_version('Gtk', '3.0') # make sure that the Gtk version is at the required level
from gi.repository import Gtk # Gtk related modules for the graphical interface
from gi.repository import Gdk
from gi.repository import GdkPixbuf

# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "locale"
# ----------- Modify this ---------- #

class Page(InstallerPage):
# create a page class derived from "InstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self, mainBuilder):
        InstallerPage.__init__(self, CURRENT_PAGE_NAME, mainBuilder) # call the super-class constructor
        self.builder.connect_signals(self)                           # connect the signals from the Gtk form to our event handlers

        # ---------- Custom code ----------- #
        resources                   = JSONOptions("config/resources.json").getData()
        self.timeZoneMap            = self.builder.get_object("timeZoneMap")
        self.pixbuf                 = ImageTools.getPixbufFromFile(resources["timeZoneMap"])
        self.currentPixbuf          = self.pixbuf
        self.timeZoneMapAspectRatio = float(self.pixbuf.get_width()) / self.pixbuf.get_height()
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place
    def locale_onTimeZoneMapResize(self, box, alloc):
        # Called when the box containing the timezone map is allocated a new size
        width  = box.get_allocated_width()
        height = box.get_allocated_height()
        if float(width)/height > self.timeZoneMapAspectRatio:
            self.currentPixbuf = ImageTools.scalePixbufByHeight(self.pixbuf, height, self.timeZoneMapAspectRatio)
        else:
            self.currentPixbuf = ImageTools.scalePixbufByWidth(self.pixbuf, width, self.timeZoneMapAspectRatio)
        ImageTools.addImageFromPixbuf(self.currentPixbuf, self.timeZoneMap)

    def locale_onButtonPressed(self, box, event):
        containerWidth       = int(box.get_allocated_width())
        containerHeight      = int(box.get_allocated_height())
        displayedImageWidth  = self.currentPixbuf.get_width()
        displayedImageHeight = self.currentPixbuf.get_height()
        if float(containerWidth)/containerHeight > self.timeZoneMapAspectRatio:
            X = event.x - int((displayedImageWidth - containerWidth) / 2)
            Y = event.y         
        else:
            X = event.x
            Y = event.y - int((containerHeight - displayedImageHeight) / 2)
        if (X > 0) and (Y > 0):
            print(ImageTools.readPixelFromPixbuf(self.currentPixbuf, X, Y))

    # CUSTOM METHODS