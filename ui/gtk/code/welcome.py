# FENIX INSTALLER
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/reborn-os-team/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# ARTWORK
# 1. Trivoxel (https://gitlab.com/TriVoxel)

# IMPORTS
from util.configuration import JSONOptions
from ui.gtk.util.page import InstallerPage, PageTools
from ui.gtk.util.image import ImageTools
from ui.gtk.util.text import TextTools

# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "welcome"
# ----------- Modify this ---------- #

class Page(InstallerPage):
# create a page class derived from "InstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self, mainBuilder):
        InstallerPage.__init__(self, CURRENT_PAGE_NAME, mainBuilder) # call the super-class constructor
        self.builder.connect_signals(self)                             # connect the signals from the Gtk form to our event handlers

        # ---------- Custom code ----------- #
        resources               = JSONOptions("config/resources.json").getData()
        self.bigLogo            = self.builder.get_object("bigLogo")
        self.pixbuf             = ImageTools.getPixbufFromFile(resources["bigLogo"])
        self.bigLogoAspectRatio = float(self.pixbuf.get_width()) / self.pixbuf.get_height()
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place
    def welcome_onRebornResize(self, box, alloc):
        # Called when the box containing the Reborn logo is allocated a new size
        width  = box.get_allocated_width()
        height = box.get_allocated_height()
        if float(width)/height > self.bigLogoAspectRatio:
            pixbuf = ImageTools.scalePixbufByHeight(self.pixbuf, height-5, self.bigLogoAspectRatio)
        else:
            pixbuf = ImageTools.scalePixbufByWidth(self.pixbuf, width-5, self.bigLogoAspectRatio)
        ImageTools.addImageFromPixbuf(pixbuf, self.bigLogo)
        
    # CUSTOM METHODS

