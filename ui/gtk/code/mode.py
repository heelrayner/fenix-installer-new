# FENIX INSTALLER
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/reborn-os-team/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# IMPORTS
from util.configuration import JSONOptions
from ui.gtk.util.page import InstallerPage, PageTools
from ui.gtk.util.image import ImageTools
from ui.gtk.util.text import TextTools

# CUSTOM IMPORTS


# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "mode"
# ----------- Modify this ---------- #

class Page(InstallerPage):
# create a page class derived from "InstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self, mainBuilder):
        InstallerPage.__init__(self, CURRENT_PAGE_NAME, mainBuilder) # call the super-class constructor
        self.builder.connect_signals(self) # connect the signals from the Gtk form to our event handlers

        # ---------- Custom code ----------- #
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place
    def mode_onModeSelected(self, button):
        if button.get_active():         
            label = button.get_label().lower()
            pageStack = self.mainBuilder.get_object("main.box.paned.stack") # get the installer's Gtk Stack. This stack stores the "pages" of our installer and shows them one at a time
            pageSettings = JSONOptions("config/pages.json")
            pageData = pageSettings.getData()

            pageData = PageTools.removePages(pageData["added_pages"], pageStack, pageData)
            listOfPages = pageData["page_lists"][label]
            pageData = PageTools.addPages(listOfPages, pageStack, self.mainBuilder, pageData)            
            PageTools.refreshPages(pageStack)
            pageSettings.setData(pageData)
            pageSettings.dump()

    # CUSTOM METHODS

