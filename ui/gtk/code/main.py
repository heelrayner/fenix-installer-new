# IMPORTS
import os # for filepath related methods
import gi # Python GObject introspection module which contains Python bindings and support for Gtk
gi.require_version('Gtk', '3.0') # make sure that the Gtk version is at the required level
from gi.repository import Gtk # Gtk related modules for the graphical interface
from util.configuration import JSONOptions
from ui.gtk.util.page import PageTools

# THE EVENT HANDLER
class Main:  

    """Specify how this particular Gtk container handles user interaction events. 
    The names of handler functions (also called `signals` in Gtk) can be assigned in `Glade` under "Signals"""

    def __init__(self, commandLineArguments):

        builder = Gtk.Builder()
        builder.add_from_file( # extract the main form from the glade file
            os.path.join(
                "ui",
                commandLineArguments.user_interface,
                "forms",
                "main.glade"
            )
        ) 
        builder.connect_signals(self) # connect the signals from the Gtk forms to our event handlers (which are all defined in a class)
        pageStack = builder.get_object("main.box.paned.stack") # get the installer's Gtk Stack. This stack stores the "pages" of our installer and shows them one at a time    
        pageData = JSONOptions("config/pages.json").getData()
        listOfPages = pageData["initial_pages"] + pageData["page_lists"][commandLineArguments.mode] 
        _ = PageTools.addPages(listOfPages, pageStack, builder, pageData)
        builder.get_object("main").show_all() # get the main form object and make it visible
        Gtk.main() # start the GUI event loop

    def main_onClose (self, mainForm):

        """Quit the graphical interface

        Called when the application is closedMainFormHandler
        This happens either when
        (1) the container object is destroyed or 
        (2) when the `Abort` button is clicked
        The arguments received in this method may have been selected inside `Glade` under the corresponding signal"""

        Gtk.main_quit()

    def main_onBackClicked (self, pageStack):

        """If the installation has not started yet, switch to the previous page

        Called when either 
        (1) the `Back` button is clicked or
        (2) the `back icon` of the headerbar is clicked
        The arguments received in this method may have been selected inside `Glade` under the corresponding signal"""

        installerData = JSONOptions("config/pages.json").getData() # read the config file for data related to pages
        isInstalling = JSONOptions("config/installer.json").getEntry("installing")
        if not isInstalling: # if the installation has not started yet
            listOfPageNames = installerData["initial_pages"] + installerData["added_pages"]
            currentPageIndex = listOfPageNames.index(pageStack.get_visible_child_name()) # Determine the index of the current visible page
            if currentPageIndex > 0: # If the currently visible page is not the first one
                pageStack.set_visible_child_name(listOfPageNames[currentPageIndex - 1]) # Select the previous page name from the list and make it visible

    def main_onNextClicked (self, pageStack): 
        
        """ If the installation has not started yet, switch to the next page
        
        Called when either 
        (1) the `Next` button is clicked or
        (2) the `Next icon` of the headerbar is clicked     
        The arguments received in this method may have been selected inside `Glade` under the corresponding signal"""
              
        installerData = JSONOptions("config/pages.json").getData() # read the config file for data related to pages     
        isInstalling = JSONOptions("config/installer.json").getEntry("installing")
        if not isInstalling: # if the installation has not started yet
            listOfPageNames = installerData["initial_pages"] + installerData["added_pages"]
            currentPageIndex = listOfPageNames.index(pageStack.get_visible_child_name()) # Determine the index of the current visible page
            if currentPageIndex < (len(listOfPageNames) - 1): # If the currently visible page is not the last one
                pageStack.set_visible_child_name(listOfPageNames[currentPageIndex + 1])  # Select the next page name from the list and make it visible

    def main_consoleResized(self, consoleScolledWindow, rectangle):

        """ Scroll down the GUI based debugging console as new text is added and the console changes in size

        Called by Gtk when the debugging console changes in size because of new text
        The arguments received in this method may have been selected inside `Glade` under the corresponding signal"""

        adjustment = consoleScolledWindow.get_vadjustment()
        adjustment.set_value(adjustment.get_upper() - adjustment.get_page_size())
        consoleScolledWindow.set_vadjustment(adjustment) # scroll to the bottom of the debugging console