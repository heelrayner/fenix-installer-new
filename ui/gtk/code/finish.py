# FENIX INSTALLER
# Please refer to the file `LICENSE` in the main directory for license information. 
# For a high level documentation, please visit https://gitlab.com/reborn-os-team/fenix-installer

# AUTHORS
# 1. Shivanand Pattanshetti (shivanand.pattanshetti@gmail.com)

# GUI
# 1. Trivoxel (https://gitlab.com/TriVoxel)

# IMPORTS
from util.configuration import JSONOptions
from ui.gtk.util.page import InstallerPage, PageTools
from ui.gtk.util.image import ImageTools
from ui.gtk.util.text import TextTools

# CUSTOM IMPORTS


# ----------- Modify this ---------- #
CURRENT_PAGE_NAME = "finish"
# ----------- Modify this ---------- #

class Page(InstallerPage):
# create a page class derived from "InstallerPage" which contains a page template

    # CONSTRUCTOR
    def __init__(self, mainBuilder):
        InstallerPage.__init__(self, CURRENT_PAGE_NAME, mainBuilder) # call the super-class constructor
        self.builder.connect_signals(self)                           # connect the signals from the Gtk form to our event handlers

        # ---------- Custom code ----------- #
        self.addFinishMessage()
        # -------- Custom code ends -------- #

    # EVENT HANDLERS
    # To specify how this particular Gtk container handles user interactions. The names of handler functions can be assigned in `Glade` under "Signals"
    # Create functions (with names as described above) that are called when specific UI events take place


    # CUSTOM METHODS
    def addFinishMessage(self):
        introductionTextBuffer = self.builder.get_object("finish_message").get_buffer()
        TextTools.addBasicTextTags(introductionTextBuffer)
        iter = introductionTextBuffer.get_start_iter()
        introductionTextBuffer.insert_with_tags_by_name(iter, "Fenix Installer has finished installing Reborn OS. Please restart your computer or continue using the live ISO.")
        introductionTextBuffer.insert_with_tags_by_name(iter, "\n\nThanks for trying out Reborn OS!", "bold")
