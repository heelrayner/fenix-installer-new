import os
import subprocess
import logging
import concurrent.futures
from concurrent.futures import ThreadPoolExecutor
from pathlib import Path

class Runnable():

    def __init__(self):
        self._future = None
        self._process = None


class Command(Runnable):
    def __init__(self, command_string:str = None) -> None:
        super().__init__()
        self.command_string = command_string

    def set_string(self, command_string:str = None) -> None:
        self._command_string = command_string

    def getString(self) -> str:
        return self.command_string

    def execute(self, timeout:float = None) -> subprocess.CompletedProcess:
        return subprocess.run(
            self._command_string,
            capture_output = True,
            shell = True,
            timeout = timeout
        )

class InstallerJob():

    _trapString = "trap \'printf \"[%s %s %s] %s\\n\" $(date \'\"\'\"\'+%F, %T %Z\'\"\'\"\') \"$BASH_COMMAND\"\' DEBUG" # bash trap command to display inputs and outputs with time stamps. Multiple nested quotes have been escaped.

    def __init__(self):
        super().__init__()
        self._thread_pool_executor = ThreadPoolExecutor(max_workers=1, thread_name_prefix='InstallerJob')
        self.runnables = []
        self._logger = logging.getLogger('fenix_installer'+'.'+str(Path(__file__).stem).replace(str(Path(str(Path.cwd())+'/')), '').replace('/','.'))

    def runShellCommand(self, command_str:str, timeout_s:float=None):
        a = self._thread_pool_executor.submit(self.__shellCommandWorker, command_str)
        concurrent.futures.wait([a], timeout=None, return_when=concurrent.futures.ALL_COMPLETED)
        a.result()

    def runShellScript(self, script_filepath:Path, timeout_s:float=None):
        a = self._thread_pool_executor.submit(self.__shellScriptWorker, script_filepath)
        concurrent.futures.wait([a], timeout=None, return_when=concurrent.futures.ALL_COMPLETED)
        a.result()

    def __shellCommandWorker(self, command_str:str):
        self._logger.debug(command_str)                    # the input is written to the log file
        process = subprocess.Popen(command_str,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True
        ) # run the command such that both the output and errors go to stdout
        while True:
            output = str(process.stdout.readline().strip().decode()) + ""
            error = str(process.stderr.readline().strip().decode()) + ""
            if output == '' and process.poll() is not None: # command completed
                # try:
                #     self._thread_pool_executor.shutdown(wait=True)
                # except RuntimeError as e:
                #     pass
                break                                                     
            if output:     
                self._logger.debug(output)
            if error:
                self._logger.error(error)

    def __shellScriptWorker(self, script_filepath:Path):
        with open(script_filepath + "_temp", "w") as script:
            with open(script_filepath, "r") as script_old:
                script.write(self._trapString + "\n\n") # create a temporary script with a trap command to display both inputs and outputs, with time stamps              
                script.write(script_old.read())          # copy all lines from the script to a temporary script
        
        process = subprocess.Popen(
            "sh " + script_filepath + "_temp",
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            shell=True
        ) # run the temporary script (with a trap command) such that both the output and errors go to stdout

        while True:                        
            output = str(process.stdout.readline().strip().decode()) + ""
            error = str(process.stderr.readline().strip().decode()) + ""
            if output == '' and process.poll() is not None: # command completed
                # try:
                #     self._thread_pool_executor.shutdown(wait=True)
                # except RuntimeError as e:
                #     pass
                break                                                     
            if output:     
                self._logger.debug(output)
            if error:
                self._logger.error(error)
        
        os.remove(script_filepath + "_temp") # remove the temporary script    
        
    def __del__(self):
        try:
            self._thread_pool_executor.shutdown(wait=True)
        except RuntimeError as e:
            pass


