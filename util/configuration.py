import json
from pathlib import Path

class JSONSettings():

    """Useful tools to read/write settings in a 'JSON' file"""

    def __init__(self, filepath:str = None) -> None:

        """
        Initializes a 'JSONSettings' object.

        If 'filepath' is specified, attempt is made to load data from it.
        If no such file exists, an error is raised.

        Parameters
        ----------
        self:JSONSettings
            An instance of the current object
        filepath:str
            Either relative or absolute path to a JSON file (with forward slashes)

        Raises
        ------
        UnboundLocalError
            _filepath was not assigned
        FileNotFoundError
            _filepath is not a valid file

        Returns
        -------
        None
        """

        self._filepath:Path = None
        self._data:dict = None

        if filepath is not None:
            self._filepath = Path(filepath)
            self.assertPathIsValid()             
            self.load()

    def assertPathIsValid(self) -> None:

        """
        Checks whether '_filepath' is a valid file path.
        If not, an error is raised.

        Parameters
        ----------
        self:JSONSettings
            An instance of the current object

        Raises
        ------
        UnboundLocalError
            _filepath was not assigned
        FileNotFoundError
            _filepath is not a valid file
                   
        Returns
        -------
        None
        """

        if self._filepath is None:
            raise UnboundLocalError("_filepath has not been assigned yet.")
        elif (not self._filepath.exists()) or (not self._filepath.is_file()):
            raise FileNotFoundError("File not found at given path: ", str(self._filepath))

    def assertDataIsValid(self) -> None:

        """
        Checks whether _data is a non-empty dict.

        Parameters
        ----------
        self:JSONSettings
            An instance of the current object

        Raises
        ------
        UnboundLocalError
            _data was not assigned
        TypeError
            _data is not a dict
                   
        Returns
        -------
        None
        """

        if self._data is None:
            raise UnboundLocalError("_data has not been assigned yet. Try calling load() to read the settings file or setData() to manually set a dict.")
        elif not isinstance(self._data, dict):
            raise TypeError("_data is not a dict, but of type: ", type(self._data))

    @staticmethod
    def _readFromFile(filepath:Path) -> dict:

        """
        Retrieves data from a JSON file at 'filepath' as a dict.

        Parameters
        ----------
        filepath:Path
            Path to the JSON configuration file which has the data

        Returns
        -------
        config_data:dict
            Data contained within the JSON file at 'filepath' as a 'dict'
        """

        with open(filepath, 'r') as config_file:
            config_data = json.load(config_file)

        return config_data

    @staticmethod
    def _writeToFile(data:dict, filepath:Path) -> None:

        """
        Writes the dict 'data' to a JSON file at 'filepath'.

        Parameters
        ----------
        data:dict
            The dict data to be written to a JSON file.
        filepath:Path
            Path to the JSON configuration file to write data

        Returns
        -------
        Nothing
        """

        with open(filepath, 'w') as config_file:
            json.dump(data, config_file, indent=4)

    def load(self) -> None:

        """
        Loads data from the JSON configuration file at '_filepath' into a dict '_data'.
       
        Parameters
        ----------
        self:JSONSettings
            An instance of the current object

        Returns
        -------
        Nothing
        """

        self.assertPathIsValid()
        self._data = JSONSettings._readFromFile(self._filepath)

    def dump(self) -> None:

        """
        Writes the dict data in '_data' to a JSON configuration file at '_filepath'.

        Parameters
        ----------
        self: JSONSettings
            An instance of the current object

        Returns
        -------
        Nothing
        """

        self.assertPathIsValid()
        self.assertPathIsValid()
        JSONSettings._writeToFile(self._data, self._filepath)

    def getEntry(self, entry_name:str):
        """
        Returns the value of the setting 'entry_name'

        Parameters
        ----------
        self:JSONSettings
            An instance of the current object
        entry_name:str
            Name of the entry that is to be retreived

        Returns
        -------
        value: str
            Value of the setting 'entry_name'
        """

        self.assertDataIsValid()
        return self._data[entry_name]

    def setEntry(self, value, entry_name:str) -> None: 
        
        """
        Sets the value for the entry 'entry_name'.
        WARNING: No actual data would be written until 'dump()' is called

        Parameters
        ----------
        self:JSONOptions
            An instance of the current object
        value:_
            the the value of the setting "entry_name"
        entry_name:str
            Name of the entry that is to be edited

        Returns
        -------
        Nothing
        """

        self.assertDataIsValid()
        self._data[entry_name] = value

    def setFilepath(self, filepath:str) -> None:

        """
        Sets the JSON configuration filepath to read or write data from

        Parameters
        ----------
        self:JSONSettings
            An instance of current object
        filepath:str
            Path to the JSON configuration file whose data is to be read or written
            
        Returns
        -------
        Nothing
        """

        self._filepath = Path(filepath)
        self.assertPathIsValid()

    def setData(self, data:dict) -> None:

        """
        Sets _data to a given dict value
        WARNING: No actual data would be written until 'dump()' is called

        Parameters
        ----------
        self:JSONSettings
            An instance of the current object
        data:dict
            Given dict to replace the value of self._data
            
        Returns
        -------
        Nothing
        """

        self._data = data

    def getFilepath(self) -> str:

        """
        Retrieve path the JSON configuration file path in _filepath

        Parameters
        ----------
        self:JSONSettings
            An instance of the current object
                   
        Returns
        -------
        filepath:str
           Path to the JSON configuration file in _filepath
        """

        self.assertPathIsValid()
        return str(self._filepath)

    def getData(self) -> dict:

        """
        Retrieve the configuration data stored as a dict in '_data'

        Parameters
        ----------
        self:JSONSettings
            An instance of the current object
                   
        Returns
        -------
        data:dict
            Configuration data stored as a dict in '_data'
        """

        self.assertDataIsValid()
        return self._data

class JSONOptions(JSONSettings):

    """
    Manage settings that allow choosing from a list, and are in the form below:

    Eg:
    "ui-toolkits": {
        "available": [
            "gtk",
            "qt",
            "qtquick"
        ],
        "default": "gtk",
        "current": ""
    }
    """

    def __init__(self, filepath:str = None) -> None:

        """
        Initialize a 'JSONOptions' object
        
        Parameters
        ----------
        self:JSONOptions
            An instance of the current object
        filepath:str
            Either relative or absolute path to a JSON file (with forward slashes)
        """

        super().__init__(filepath)

    def assertEntryIsSelectable(self, entry_name:str) -> bool:

        """
        Tells whether the given entry is defined as a selectable one in the configuration file. 
        A selectable entry is at top level (if nested) and strictly looks like 'modes' below:
        
        "modes": {
            "available": [
                "silent_minimal",
                "silent_basic",
                "silent_full",
                "minimal",
                "basic",
                "full",
                "semi-automatic",
                "customize"
            ],
            "default": "semi-automatic",
            "current": ""
        }

        Parameters
        ----------
        self:JSONOptions
            An instance of the current object
        entry_name:str
            Name of the entry to be checked

        Raises
        -------
        TypeError
            Entry not in the above-defined "selectable" format
        KeyError
            Entry not found, or is not one of the keys in the dict data
        """

        self.assertDataIsValid()

        error_message = """
        The provided entry_name is not a valid "selectable" setting.
        A selectable setting looks like the following:
        "modes": {
            "available": [
                "silent_minimal",
                "silent_basic",
                "silent_full",
                "minimal",
                "basic",
                "full",
                "semi-automatic",
                "customize"
            ],
            "default": "semi-automatic",
            "current": ""
        }
        """
        if entry_name in self._data:
            if (
                ("available" not in self._data[entry_name])
                or (not isinstance(self._data[entry_name]["available"], list))
                or ("default" not in self._data[entry_name])
                or ("current" not in self._data[entry_name])
            ):
                raise TypeError(error_message)
            else:
                return
        else:
            raise KeyError("The provided entry_name is not found as a key in _data")

    def getAvailableChoices(self, entry_name:str) -> list: 

        """
        Returns the list of available "choices" for the "selectable" setting denoted by "entry_name"

        Parameters
        ----------
        self:JSONOptions
            An instance of the current object
        entry_name:str
            The name of the entry that is to be retrieved

        Returns
        -------
        listOfChoices:list(str)
            the list of choices for the given selectable setting
        """

        self.assertEntryIsSelectable(entry_name)
        return self._data[entry_name]["available"]

    def getDefaultChoice(self, entry_name:str):

        """
        Returns the default "choice" for the "selectable" setting denoted by "entry_name"

        Parameters
        ----------
        self:JSONOptions
            An instance of the current object
        entry_name:str
            The name of the entry that is to be retrieved

        Returns
        -------
        defaultChoice:_
            the default choice for the given selectable setting
        """

        self.assertEntryIsSelectable(entry_name)
        return self._data[entry_name]["default"]

    def getCurrentChoice(self, entry_name:str):

        """
        Returns the current "choice" for the "selectable" setting denoted by "entry_name"

        Parameters
        ----------
        self:JSONOptions
            An instance of the current object
        entry_name:str
            The name of the entry that is to be retrieved

        Returns
        -------
        currentChoice:_
            the current choice for the given selectable setting"""

        self.assertEntryIsSelectable(entry_name)
        return self._data[entry_name]["current"]

    def setAvailableChoices(self, availableChoices:list, entry_name:str) -> None: 
        
        """
        Sets the list of available "choices" for the "selectable" setting denoted by "entry_name".
        WARNING: No actual data is written until "dump()" is called

        Parameters
        ----------
        self:JSONOptions
            An instance of the current object
        availableChoices:list(str)
            the list of choices for the given selectable setting
        entry_name:str
            The name of the entry that is to be modified

        Returns
        -------
        Nothing
        """

        self.assertDataIsValid()
        self._data[entry_name]["available"] = availableChoices

    def setDefaultChoice(self, defaultChoice, entry_name:str) -> None: 
        
        """
        Sets the default "choice" for the "selectable" setting denoted by "entry_name".
        WARNING: No actual data is written until "dump()" is called

        Parameters
        ----------
        self:JSONOptions
            An instance of the current object
        defaultChoice:_
            the default choice for the given selectable setting
        entry_name:str
            The name of the entry that is to be modified

        Returns
        -------
        Nothing
        """

        self.assertDataIsValid()
        self._data[entry_name]["default"] = defaultChoice

    def setCurrentChoice(self, currentChoice, entry_name:str) -> None: 
        
        """
        Sets the current "choice" for the "selectable" setting denoted by "entry_name".
        WARNING: No actual data would be written until "dump()" is called

        Parameters
        ----------
        self:JSONOptions
            An instance of the current object
        currentChoice: _
            the current choice for the given selectable setting
        entry_name:str
            The name of the entry that is to be checked

        Returns
        -------
        Nothing
        """

        self.assertDataIsValid()        
        self._data[entry_name]["current"] = currentChoice