#! /usr/bin/python

import os
import logging
import argparse
import importlib
import datetime
from pathlib import Path
from util.configuration import JSONOptions

def _main():

    """
    The main function.

    - Configure logging
    - Set the current working directory,
    - Define and parse command line options
    - Start the installer based on command line options
    """ 

    _ = _setupLogger() # configure logger
    installerSettings = JSONOptions("config/installer.json") # to access the settings stored in 'installer.json'
    _setCurrentWorkingDirectory(installerSettings) # make the base directory of the installer as the current working directory       
    commandLineArguments = _handleArguments(installerSettings) # handle command line arguments
    _loadUI(commandLineArguments) # load the user interface

def _setupLogger():

    """
    Configure the logger

    - Create a named logger
    - Setup logging to be done onto a file and the console
    - Define the format of log entries
    """

    logger = logging.getLogger('fenix_installer') # create a new logger and name it
    logger.setLevel(logging.DEBUG) # log debug messages and higher
    
    logFilePath = Path("log/" + getTimeStamp() + ".txt")
    fileHandler = logging.FileHandler(logFilePath) # for logging onto files
    fileHandler.setLevel(logging.DEBUG) # log debug messages and higher

    streamHandler = logging.StreamHandler() # for logging onto the console
    streamHandler.setLevel(logging.ERROR) # log error messages and higher

    logFormatter = logging.Formatter('[%(asctime)s %(name)s] [%(levelname)s] %(message)s', '%Y-%m-%d, %H:%M:%S %Z') # format of each log entry
    fileHandler.setFormatter(logFormatter)
    streamHandler.setFormatter(logFormatter)

    logger.addHandler(fileHandler)
    logger.addHandler(streamHandler)

    return logger

def _setCurrentWorkingDirectory(installerSettings:JSONOptions): 

    """
    Set the current working directory
    
    - Set the current working directory of this script to the directory in which this file exists (i.e. the base directory of the installer)
    - Store the directory information in a configuration file
    """

    os.chdir( # change the current working directory to
        os.path.dirname( # get the directory's name for the file
            os.path.realpath( # get the full path of
                __file__ # the current file
            )
        )
    )

    installerSettings.setEntry( # store the working directory
        os.getcwd(), # the current working directory
        "currentWorkingDirectory" # name of the entry in the configuration file
    )
    installerSettings.dump() # write pending changes to the configuration file
   
def _handleArguments(installerSettings:JSONOptions):

    """Define and parse command line options"""

    argumentParser = argparse.ArgumentParser(description= 'Run Fenix Installer.') # define a parser for command line arguments
    argumentParser.add_argument( # define a command line argument for selecting UI toolkits
        '-ui', '--user_interface',
        choices= installerSettings.getAvailableChoices("ui-toolkits"),
        default= installerSettings.getDefaultChoice("ui-toolkits"),
        help= "specify the UI toolkit."
    )
    argumentParser.add_argument( # define a command line argument for selecting the installation mode
        '-m', '--mode',
        choices= installerSettings.getAvailableChoices("modes"),
        default= installerSettings.getDefaultChoice("modes"),
        help= "specify the mode of installation."
    )
    return argumentParser.parse_args()

def _loadUI(commandLineArguments):

    installerMode = commandLineArguments.mode

    pageSettings = JSONOptions("config/pages.json") # to access installer page configuration data
    pageSettings.setEntry(
        pageSettings.getData()["page_lists"][installerMode],
        "added_pages"
    )
    pageSettings.dump()
       
    main = importlib.import_module("ui" + "." + commandLineArguments.user_interface + "." + "code" + "." + "main") # search for and import ui/<ui_toolkit>/code/main.py
    _ = main.Main(commandLineArguments) # initialize the Main class of the main script for the chosen user interface toolkit

def getTimeStamp() -> str:

    return (
        datetime.datetime.now().strftime("%Y-%m-%d_%H_%M_%S_")
        + datetime.datetime.now(datetime.timezone.utc).astimezone().tzname()
    )

# THE EXECUTION STARTS HERE
if __name__ == '__main__': 
    _main()

