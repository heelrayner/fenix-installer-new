#!/bin/bash

DIRECTORY=/var/log

function disks() {
    # Push all disks into disks.txt
    echo $(fdisk -l | grep "Disk /dev/sd" | awk '{print $2}' | tr -d :) >$DIRECTORY/disk.txt
    tr -s ' '  '\n'< $DIRECTORY/disk.txt >$DIRECTORY/disks.txt
    rm -f $DIRECTORY/disk.txt
}

function unallocated() {
    IFS=$'\n'       # make newlines the only separator
    set -f          # disable globbing

    # Push the largest slot of unnallocated space (from now on referred to as "free space") from each disk into partitions.txt
    for i in $(cat < $DIRECTORY/disks.txt); do
        if [[ $(parted $i unit gb print free | grep "Free Space" | awk '{print $3}' | cut -f1 -d "." | tr -d "GB" | sort -rn | head -n 1) -ge 20 ]]; then
            echo $(parted $i unit gb print free | grep "Free Space" | awk '{print $3}' | tr -d "GB" | sort -rn | head -n 1) >>$DIRECTORY/partitions.txt
            echo "Unnallocated space of 20 GB or larger HAS been located on $i!"
        else
            echo "Unnallocated space of 20 GB or larger has NOT been located on $i."
        fi
    done
    if [ -f $DIRECTORY/partitions.txt ]; then

        # Push the largest free space across all disks into partition.txt
        echo $(cat $DIRECTORY/partitions.txt | sort -rn | head -n1) > $DIRECTORY/partition.txt

        # Narrow down the list of disks from disks.txt to the one with the free space,
        # inputting that into disk.txt
        partition_used=$(head -n 1 $DIRECTORY/partition.txt)
        for j in $(cat < $DIRECTORY/disks.txt); do
            if [[ $(parted $j unit gb print free | grep "Free Space" | grep $partition_used | wc -l) -eq 1 ]]; then
                echo $j > $DIRECTORY/disk.txt
            fi
        done

        # Push the start and end locations of the free space to partition.txt
        disk_used=$(head -n 1 $DIRECTORY/disk.txt)
        echo $(parted $disk_used unit gb print free | grep "Free Space" | grep $partition_used | awk '{print $1}') >>$DIRECTORY/partition.txt
        echo $(parted $disk_used unit gb print free | grep "Free Space" | grep $partition_used | awk '{print $2}') >>$DIRECTORY/partition.txt
        return 0
    else
        return 1
    fi
}

# All this function does is return a `1` if there is an UEFI partition on the disk the system is being installed to,
# a `2` is there is an UEFI partition on another disk other than the one the system is being installed to,
# and a `0` if there is none at all.
function detect_uefi() {
    disk_used=$(head -n 1 $DIRECTORY/disk.txt)
    if [[ $(fdisk -l 2>&1 2>/dev/null | grep "EFI System" | grep $disk_used | awk '{print $1}' | wc -l) -gt 0 ]]; then
        echo "An UEFI partition is present on the disk $disk_used :)"
        return 1
    elif [[ $(fdisk -l 2>&1 2>/dev/null | grep "EFI System" | grep -v $disk_used | awk '{print $1}' | wc -l) -gt 0 ]]; then
        echo "An UEFI partition is present on a different disk than $disk_used"
        return 2
    elif [[ $(efibootmgr | wc -l) -gt 0 ]]; then
        echo "No UEFI partition exists, but your hardware is compatible with it"
        return 3
    else
        echo "Your hardware is not compatible with UEFI"
        return 0
    fi
}

function uefi_check_table() {
    disk_used=$(head -n 1 $DIRECTORY/disk.txt)
    # Output whether or not the device table is gpt. If not, return error code 1 to alert the script
    # that even though the device is UEFI-capable, the disk is NOT formated for that at all.
    if [[ $(parted $disk_used unit gb print | grep "Partition Table" | grep -o gpt | wc -l) -le 0 ]]; then
        return 1
    else
        return 0
    fi
}

# If the function `uefi` returned a `1`, mount the existing UEFI partition, but do NOT format it
function mount_uefi() {
    uefi_partition=$(fdisk -l 2>&1 2>/dev/null | grep "EFI System" | grep $disk_used | awk '{print $1}')

    # Mount the new uefi partition
    mount $uefi_partition /boot/efi

    # Push the partition name into uefi_partition.txt
    echo $(fdisk -l 2>&1 2>/dev/null | grep "EFI System" | grep $disk_used | awk '{print $1}') >$DIRECTORY/uefi_partition.txt

    # Add UEFI partition to fstab
    uefi_uuid=$(blkid $uefi_partition | awk '{print $2}' | tr -d '\"')
    echo "$uefi_uuid           	/boot/efi         	vfat      	defaults,noatime 0 2" >>/mnt/etc/fstab
}

function create_uefi() {
    # Assign necessary variables in regards to the free space
    disk_used=$(head -n 1 $DIRECTORY/disk.txt)
    partition_start_GB=$(sed -n '2p' < $DIRECTORY/partition.txt)
    partition_start=$(sed -n '2p' < $DIRECTORY/partition.txt | tr -d GB)
    partition_used=$(head -n 1 $DIRECTORY/partition.txt)

    # Change unit to MB if $partition_start is smaller than one, in order to make parted happy
    partition_start_temp=$(printf "%.0f\n" "$partition_start")
    partition_start=$partition_start_temp
    if [[ $partition_start -eq 0 ]]; then
        partition_end="300"
        unit=MB
    else
        partition_end="0.30"
        unit=GB
    fi

    # Create uefi partition from the free space
    yes "I" | parted $disk_used mkpart esp $partition_start$unit -- $(awk "BEGIN {print $partition_start+$partition_end; exit}")$unit

    # Reassign partition_start to the value that the root partition will be starting at
    partition_root_start=$(awk "BEGIN {print $partition_start+$partition_end; exit}")
    partition_root_start_noGB=$partition_root_start
    partition_root_start+=$unit

    # Push the new starting position for the remaining unallocated space to the second line of partition.txt
    sed -i "2s/.*/$partition_root_start/" $DIRECTORY/partition.txt

    # Assign partition_start to the new starting location
    partition_root_start=$(sed -n '2p' < $DIRECTORY/partition.txt)

    # Update the partition_used variable
    echo $(parted $disk_used unit gb print free | grep "Free Space" | awk '{print $3}' | tr -d "GB" | sort -rn | head -n 1) >$DIRECTORY/partitions.txt

    # Unsure all units are converted back to GB in the event they were previously changed to MB earlier
    if [[ $partition_start -eq 0 ]]; then
        partition_end="0.30"
        partition_start="0.00"
        unit=GB
    else
        partition_end=$partition_root_start_noGB
        partition_start=$(sed -n '2p' < $DIRECTORY/partition.txt | tr -d GB)
    fi
    # Assign number of partition to variable partition_number
    partition_number=$(parted $disk_used unit gb print | grep $partition_start | grep $partition_end | awk '{print $1}')
    sed -i "1s/.*/$(cat $DIRECTORY/partitions.txt | sort -rn | head -n1)/" $DIRECTORY/partition.txt

    # Convert newly created partition from the defaul ext4 to fat32
    mkfs.vfat -F 32 $disk_used$partition_number

    # Enable the new uefi partition by setting the necessary flags for it
    parted $disk_used set $partition_number esp on
}

function detect_bios() {
    disk_used=$(head -n 1 $DIRECTORY/disk.txt)
    if [[ $(fdisk -l 2>&1 2>/dev/null | grep "boot" | grep $disk_used | awk '{print $1}' | wc -l) -gt 0 ]]; then
        echo "A BIOS booting partition is present on the disk $disk_used :)"
        return 1
    elif [[ $(fdisk -l 2>&1 2>/dev/null | grep "boot" | grep -v $disk_used | awk '{print $1}' | wc -l) -gt 0 ]]; then
        echo "A BIOS booting partition is present on a different disk than $disk_used"
        return 2
    elif [[ $(efibootmgr | wc -l) -eq 0 ]]; then
        echo "No BIOS partition exists, but your hardware is compatible with it"
        return 3
    else
        echo "Your hardware is not compatible with BIOS"
        return 0
    fi
}

function bios_check_table() {
    # Output whether or not the device table is mbr. If not, return error code 1 to alert the script
    # that even though the device is BIOS-capable, the disk is NOT formated for that at all.
    disk_used=$(head -n 1 $DIRECTORY/disk.txt)
    if [[ $(parted $disk_used unit gb print | grep "Partition Table" | grep -o msdos | wc -l) -le 0 ]]; then
        return 1
    else
        return 0
    fi
}

function create_bios_flag() {
    # Assign necessary variables in regards to the free space
    disk_used=$(head -n 1 $DIRECTORY/disk.txt)
    partition_start=$(sed -n '2p' < $DIRECTORY/partition.txt)
    partition_end=$(sed -n '3p' < $DIRECTORY/partition.txt)
    partition_number=$(parted $disk_used unit gb print | grep $partition_start | grep $partition_end | awk '{print $1}')

    # Create boot flag for partition that is being used for the installation
    parted $disk_used set $partition_number boot on
}

function format() {
    # Assign necessary variables in regards to the free space
    disk_used=$(head -n 1 $DIRECTORY/disk.txt)
    partition_used=$(head -n 1 $DIRECTORY/partition.txt)
    partition_start=$(sed -n '2p' < $DIRECTORY/partition.txt)
    partition_end=$(sed -n '3p' < $DIRECTORY/partition.txt)

    # Output the location and size of the free space
    echo "Congrats! Unnallocated space has been found, on $(head -n 1 $DIRECTORY/disk.txt), being $(head -n 1 $DIRECTORY/partition.txt) GB in size"

    # Create primary partition from the free space
    yes "I" |   parted $disk_used mkpart primary ext4 $partition_start -- $partition_end

    # Format that new partition as ext4
    partition_number=$(parted $disk_used unit gb print | grep $partition_used | awk '{print $1}')
    echo "y" | mkfs.ext4 $disk_used$partition_number

    # Mount the new partition
    mount $disk_used$partition_number /mnt

    # Push the partition name into root_partition.txt
    echo $disk_used$partition_number >$DIRECTORY/root_partition.txt
}

function installBasics() {
    # Pacstrap base and base-devel package groups into free space
    pacstrapping(){
        pacstrap /mnt base base-devel
        touch /mnt/pacstrapped.txt
    }
    export -f pacstrapping

    # Fork the pacstrapping process to ensure that the genfstab command
    # does not end up being run in a fully chrooted environment.
    pacstrapping &
    while [[ ! -f /mnt/pacstrapped.txt ]]; do
        sleep 1
    done
    rm -f /mnt/pacstrapped.txt
    genfstab /mnt -U >>/mnt/etc/fstab
}

function swapfile() {
    # Create file /swapfile
    arch-chroot /mnt dd if=/dev/zero of=/swapfile bs=1024 count=524288
    chmod 600 /mnt/swapfile
    mkswap /mnt/swapfile

    # Activate the swap file
    arch-chroot /mnt swapon /swapfile
    echo "/swapfile swap swap defaults 0 0" >>/mnt/etc/fstab

    # Remove any previous swap entries in /etc/fstab that are no longer valid
    grep -v none /mnt/etc/fstab >/mnt/etc/fstab2
    mv /mnt/etc/fstab2 /mnt/etc/fstab
}

# Remove files that contain information that is no longer needed for Fenix.
# FILES THAT ARE STILL AROUND:
#   - $DIRECTORY/disk.txt
#   - $DIRECTORY/root_partition.txt
#   - $DIRECTORY/uefi_partition.txt

function cleanUp() {
    rm -f $DIRECTORY/disks.txt
    rm -f $DIRECTORY/partition.txt
    rm -f $DIRECTORY/partitions.txt
}

export -f unallocated detect_uefi uefi_check_table mount_uefi create_uefi detect_bios bios_check_table create_bios_flag disks format swapfile cleanUp

# SCRIPT BEGINS RUNNING HERE
disks
unallocated
if [[ $? -eq 0 ]]; then
    detect_uefi
    uefi_status=$?
    if [[ $uefi_status -eq 1 ]]; then
        uefi_check_table
        if [[ $? -eq 1 ]]; then
            exit 1
        fi
        mount_uefi
    elif [[ $uefi_status -ge 2 ]]; then
        uefi_check_table
        if [[ $? -eq 1 ]]; then
            exit 1
        fi
        create_uefi
        mount_uefi
     fi
    format
    detect_bios
    bios_status=$?
    if [[ $? -ge 2 ]]; then
        bios_check_table
        if [[ $? -eq 1 ]]; then
            exit 1
        fi
        create_bios_flag
     fi
    installBasics
    swapfile
fi
cleanUp
